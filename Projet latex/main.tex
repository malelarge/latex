\documentclass{article}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{geometry}


\title{Le triangle de Pascal}
\author{Matéo Lelarge Lucas Villeneuve}
\date{10 janvier 2024}
\geometry{left=1in, right=1in}

\begin{document}

\maketitle

\vfill
\tableofcontents
\clearpage

\section{Introduction}
Cela peut ressembler à un empilement de nombres bien rangés, mais c'est en fait un trésor mathématique. Les mathématiciens indiens l'appelaient "L'escalier du mont Meru" \footnote{Montagne mythique dans la cosmogonie hindoue}, en Iran, il est appelé "Triangle de Khayyam" et en Chine, il s'appelle "Triangle de Yang Hui". Pour une grande partie du monde occidental, il est connu sous le nom de "Triangle de Pascal",d'après le mathématicien français Blaise Pascal; ce qui semble un peu injuste car il est clairement arrivé après la bataille, même s'il avait encore beaucoup à apporter.

Alors, pourquoi ce triangle a intrigué les mathématiciens du monde entier ?

\begin{figure*}[htbp]
    \begin{minipage}[b]{0.3\textwidth}
        \centering
        \includegraphics[width=3cm]{khayyam.jpg}
        \caption{Khayyam}
        \label{Khayyam}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.3\textwidth}
        \centering
        \includegraphics[width=3cm]{Yang Hui.jpg}
        \caption{Yang Hui}
        \label{Yang Hui}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.3\textwidth}
        \centering
        \includegraphics[width=3.47cm]{blaisepascal.jpg}
        \caption{Pascal}
        \label{Pascal}
    \end{minipage}
\end{figure*}

\section{Construction}
Pour réaliser un tel outil mathématique, on procédera par récurrence. On commence par noter un 1 sur la première ligne. Pour chaque ligne du dessous, on additionne les deux valeurs du dessus, tout en imaginant un zéro à gauche de la première valeur et à droite de la dernière valeur de chaque ligne.

\begin{figure*}[htbp]
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{Triangle de pascal.png}
        \caption{Triangle de Pascal}
        \label{Triangle}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{Triangle de pascal 2.png}
        \caption{Triangle de Pascal}
        \label{Triangle}
    \end{minipage}
\end{figure*}

    Ce triangle est régit par les formules que Pascal à trouvé: $\binom{n}{k} = \binom{n-1}{k-1} - \binom{n-1}{k}$.
    On rappelle que $\binom{n}{0} = \binom{n}{n} = 1$ et que $\binom{n}{1} = k$.

    Traduit en français, cela signifie que chaque valeur du triangle (excepté le 1 du haut) est la somme de la valeur au-dessus et de la valeur au-dessus et à gauche.

    Précisons aussi qu'il est possible de créer un tétraèdre de Pascal (pyramide dont la base est un triangle équilatéral), construit de la même manière, dont voici une illustration:
    
\begin{figure*}[htbp]
    \begin{minipage}[b]{1\textwidth}
        \centering
        \includegraphics[width=4cm]{pyramide pascal.png}
        \caption{Pyramide de Pascal}
        \label{Triangle}
    \end{minipage}
\end{figure*}

\section{Particularités}
    \subsection{Coefficient binomial}
        Pour chaque ligne, les valeurs correspondent aux coefficients du développement de la forme $(x+y)^n$, où x et y appartiennent à $\mathbb{R}$ et n appartient à $\mathbb{N}$, en commençant à partir de 0. 
        \\
        Par exemple, pour n = 2, on trouve: 
        \begin{equation}
            (x+y)^2 = 1*x^2 + 2*x*y + 1*y^2
        \end{equation}
        Et la ligne correspondante vaut 1 2 1;
        \\
        Pour $(x+y)^3$, on trouve:
        \begin{equation}
            (x+y)^3 = 1*x^3 +3*x^2*y + 3*x*y^2 +1*y^3
        \end{equation}
        et la ligne correspondante vaut 1 3 3 1.
        \\
        On retrouve bien la formule utilisée en cours:
        \begin{equation}
            (x+y)^n = \sum_{k=0}^n \binom{n}{k} * x^{n-k} * y^k 
        \end{equation}
        \\
        Cette égalité s'appelle aussi "La formule du binôme de Newton".
    \subsection{Décomposition en base décimale}
        En faisant la décomposition en décimal de chaque ligne, c'est à dire que le dernier élément est multiplié par $10^0$, l'avant-dernier par $10^1$, celui d'avant par $10^2$, etc ..., nous trouvons pour les premières lignes, respectivement 1, 11, 121 et 1331 qui sont 11 à la puissance numéro de ligne.
        \\
        Par exemple, pour la ligne avec 6, on a:
        $1*10^0 + 6*10^1 + 15*10^2 + 20*10^3 + 20*10^4 + 15*10^5 + 6*10^6 + 1*10^7 = 1771561 = 11^6$.
    \subsection{Somme de chaque ligne}
        Faisons maintenant la somme des valeurs de chaque ligne; on obtient les résultats suivants: 1, 2, 4, 8, 16, etc ... .
        On remarque que ce sont les valeurs de la forme $2^n$ où n est le numéro de la ligne, à partir de 0.
    \subsection{Applications géométriques}
        Lorsqu'on regarde les diagonales du Triangle de Pascal, on remarque d'abord une symétrie. En les analysant plus en détails, la première est entièrement faite de 1 et la deuxième est la suite des entiers naturels positifs. Celles-ci ne sont pas très intéressantes, contrairement à la troisième qui est la suite des nombres triangulaires: 1, 3, 6, 10 etc ..., car on peut les empiler sous forme d'un triangle équilatéral.       
        Aussi, la quatrième diagonale est la suite des nombres tétraédriques: 1, 4, 10, 20 etc ... .On peut les empiler sous forme de tétraèdre.
\begin{figure*}[htbp]
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{Nombres tri et tetra.png}
        \caption{Nombres triangulaires}
        \label{nb tri}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{nombre tetra.png}
        \caption{Nombres tétaédriques}
        \label{nb tétra}
    \end{minipage}
\end{figure*}
        \\
        On peut continuer avec les nombres pentaédriques, hexaédriques etc..., en fonction de la diagonale que l'on regarde.
    \subsection{Lien avec une fractale}
        Les fractales sont des objets mathématiques qui présente des structures similaires à elles-même à toutes les échelles. Ici, si on noircit les valeurs impaires sur un nombre de lignes assez conséquent, on a un aperçu du "Triangle de Sierpinski".
\begin{figure*}[htbp]
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{Sierpinski.jpg}
        \caption{Waczaw Sierpinski}
        \label{nb tri}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=3cm]{SierpinskiTriangle.svg.png}
        \caption{Triangle de Sierpinski}
        \label{nb tétra}
    \end{minipage}
\end{figure*}
        Maintenant, reste à savoir si Sierpinski s'est servi des travaux de Pascal pour réaliser son oeuvre ou s'il n'y a aucun lien entre les deux.
    \\
    \section{Utilisations}
        \subsection{Probabilités}
            Imaginons, que l'on veuille avoir 5 enfants: 3 filles et 2 garçons (pas forcément dans cet ordre précis). Pour savoir quelle est la probabilité que cet évènement soit un succès, on peut utiliser le Triangle de Pascal. En effet, si on regarde la ligne 5, les valeur correspondent aux coefficients de la formule $(x+y)^5$, qui valent:
            \\
            \begin{equation}
                (x+y)^5 = 1*x^5 + 5*x^4*y^1 + 10*x^3*y^2 + 10*x^2*y^3 + 5*x^1*y^4 + 1*y^5
            \end{equation}
            \\
            Ici, x est le nombre de filles et y, le nombre de garçons souhaité.e.s. Puisque nous cherchons x = 3 et y = 2, la valeur d'indice 3 de la ligne indice 5 nous donne le nombre de possibilités: 10. Pour avoir le pourcentage, on met ce résultat sur la somme de la ligne ce qui nous donne $10/32 = 31,25\%$ de chance d'avoir notre famille souhaitée.
        \subsection{Combinatoire}
            Maintenant, nous avons 12 ami.e.s et nous devons en choisir 5 pour partir en vacances. En utilisant le formule de la combinaison sans répétition, notée:
            \begin{equation}
                C^n_k = \frac{n!}{(n-k)! k!}
                \footnote{Le symbole ! est la notation de la factorielle de n. La factorielle est le produit des entiers positifs de 1 à n.}
            \end{equation}
            et avec n = 12 et k = 5, on obtient le résultat suivant:
            \begin{equation}
                \frac{12!}{7! * 5!} = \frac{12*11*10*9*8}{5*4*3*2} = 11*9*8 = 792
            \end{equation}
            Mais pour aller plus vite, on peut regarder le sixième élément de la treizième ligne du triangle, cette valeur étant bien 792.
        \subsection{Arbres binaires}
            Un arbre binaire est un arbre qui possède au maximum deux sous-arbres (ou parents). Un arbre est constitué de sommets, reliés par des arêtes et dont le sommet n'ayant pas de parents est la racine.
            \\
            Lorsqu'on relie une valeur du triangle de Pascal aux deux valeurs du dessus, on peut arriver à créer un arbre binaire. Ici, sur n'importe quelle ligne, la valeur est aussi le nombre de chemins \footnote{Un chemin valide consiste à aller soit en bas à gauche, soit en bas à droite} différents que l'on peut utiliser pour y arriver.
\begin{figure*}[htbp]
    \begin{minipage}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=3cm]{arbre binaire (random).png}
        \caption{Arbre binaire aléatoire}
        \label{arbre binaire}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=5cm]{arbre_binaire_pascal.svg.png}
        \caption{Arbre binaire de Pascal}
        \label{chemin_n4}
    \end{minipage}
\end{figure*}
        \subsection{Tours de Hanoï}
            Imaginons maintenant le problème des \href{https://fr.wikipedia.org/wiki/Tours_de_Hanoï}{Tours de Hanoï} avec un seul disque sur le piquet A, alors on peut utiliser le triangle de Pascal pour savoir quels mouvements nous sont autorisés. On prend le triangle jusqu'à la ligne 1 et on relie les trois sommets. On obtient les graphiques ci-dessous\ref{graph Hanoï}.
            \\
            On remarque qu'on peut passer directement tous les disques du piquet A au piquet C en allant en ligne droite, du sommet A au sommet C, alors pourquoi créer le sommet B ? C'est dans le cas où, dans le problème, on oblige à déplacer un disque vers un piquet adjacent (A vers B puis B vers C, au lieu de A vers C)\ref{new graph Hanoï}.
\begin{figure*}[htbp]
    \begin{minipage}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=5cm]{graph_hanoi.PNG}
        \caption{Graphique des Tours de Hanoï}
        \label{graph Hanoï}
    \end{minipage}
    \hfill
    \begin{minipage}[b]{0.5\textwidth}
        \centering
        \includegraphics[width=5cm]{graph_4.PNG}
        \caption{Graphique avec les nouvelles règles}
        \label{new graph Hanoï}
    \end{minipage}
\end{figure*}
        \subsection{Généralisation dans les nombres négatifs}
            Voici le tableau du Triangle de Pascal avec $n >= 0$
            \\
\begin{tabular}{c|ccccc}
& $m=0$ & $m=1$ & $m=2$ & $m=3$ \\
\hline
$n=0$ & 1 & & & \\
$n=1$ & 1 & 1 & & \\
$n=2$ & 1 & 2 & 1 &  \\
$n=3$ & 1 & 3 & 3 & 1 \\
\end{tabular}
            \newline
            Étendons donc la valeur n jusqu'à -3. Pour avoir les résultats, on ré-utilise la formule de Newton, mais de façon réarrangée:
            \begin{equation}
                \binom{n-1}{k} = \binom{n}{k} - \binom{n-1}{k-1}
            \end{equation}
            Notre tableau ressemble maintenant à celui-ci:
            \\
\begin{tabular}{c|ccccc}
& $m=0$ & $m=1$ & $m=2$ & $m=3$ \\
\hline
$n=-3$ & 1 & -3 & 6 & -10\\
$n=-2$ & 1 & -2 & 3 & -4 \\
$n=-1$ & 1 & -1 & 1 & -1\\
$n=0$ & 1 & 0 & 0 & 0 \\
$n=1$ & 1 & 1 & 0 & 0 \\
$n=2$ & 1 & 2 & 1 & 0 \\
$n=3$ & 1 & 3 & 3 & 1 \\
\end{tabular}
            \\
            Cette extension préserve les formules suivantes:
            \\
            ${n \choose m} = \frac{1}{m!}\prod_{k=0}^{m-1} (n-k) = \frac{1}{m!}\prod_{k=1}^{m} (n-k+1)$ et ${\displaystyle (1+x)^{n}=\sum _{k=0}^{\infty }{n \choose k}x^{k}\quad {\textrm {pour}}|x|<1}$
            \\
            Ainsi, on a :
            \\
            ${\displaystyle (1+x)^{-2}=1-2x+3x^{2}-4x^{3}+...\quad {\textrm {pour}}|x|<1}$
            \\
            En appliquant les mêmes règles que précédemment, le tableau devient:
            \\
\begin{tabular}{c|cccccccc}
& $m=-3$ & $m=-2$ & $m=-1$ & $m=0$ & $m=1$ & $m=2$ & $m=3$ \\
\hline
$n=-3$ & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
$n=-2$ & -2 & 1 & 0 & 0 & 0 & 0 & 0\\
$n=-1$ & 1 & -1 & 1 & 0 & 0 & 0 & 0\\
$n=0$ & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
$n=1$ & 0 & 0 & 0 & 1 & 1 & 0 & 0 \\
$n=2$ & 0 & 0 & 0 & 1 & 2 & 1 & 0 \\
$n=3$ & 0 & 0 & 0 & 1 & 3 & 3 & 1 \\
\end{tabular}
            \\
            Cette généralisation permet de conserver la propriété exponentielle d'une matrice. En effet, comme on a:
            ${\displaystyle \exp {\begin{pmatrix}.&.&.&.\\1&.&.&.\\.&2&.&.\\.&.&3&.\end{pmatrix}}={\begin{pmatrix}1&.&.&.\\1&1&.&.\\1&2&1&.\\1&3&3&1\end{pmatrix}}}$
            \\
            On peut étendre à ceci:
            ${\displaystyle \exp {\begin{pmatrix}.&.&.&.&.&.&.&.\\-3&.&.&.&.&.&.&.\\.&-2&.&.&.&.&.&.\\.&.&-1&.&.&.&.&.\\.&.&.&0&.&.&.&.\\.&.&.&.&1&.&.&.\\.&.&.&.&.&2&.&.\\.&.&.&.&.&.&3&.\end{pmatrix}}={\begin{pmatrix}.&.&.&.&.&.&.&.\\-1&.&.&.&.&.&.&.\\-3&1&.&.&.&.&.&.\\3&-2&1&.&.&.&.&.\\-1&1&-1&1&.&.&.&.\\.&.&.&.&1&.&.&.\\.&.&.&.&1&1&.&.\\.&.&.&.&1&2&1&.\\.&.&.&.&1&3&3&1\end{pmatrix}}}$

\section{Conclusion}
En conclusion, le Triangle de Pascal se révèle être un outil mathématique puissant et polyvalent, avec des applications étendues en probabilités, combinatoire, théorie des nombres, géométrie, et même dans des domaines plus avancés tels que la théorie des graphes et les fractales. Sa construction simple, basée sur des règles récursives, cache des propriétés fascinantes qui ont captivé l'attention des mathématiciens à travers les siècles.


\bibitem{}
            Sources: Youtube, Wikipedia, article Pour la science n°457 - novembre 2015
\end{document}